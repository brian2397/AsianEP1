#ifndef IMAGE_PGM_HPP
#define IMAGE_PGM_HPP
#include <string>
#include <vector>
#include "image.hpp"

using namespace std;

struct commentKey2{
  int begin2, messageSize2, N;
};

class imagePGM: public Image{
private:
  commentKey2 chave2;
  vector<int> position_message2;
  vector<char> vectorData2;
  vector<char> message2;
public:
  void imprimeDados();
  void readData(string filename);
  void writeImage();
  void decodeComment();
  void decodeImage();
};
#endif
