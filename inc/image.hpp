#ifndef IMAGE_HPP
#define IMAGE_HPP
#include <string>

using namespace std;

class Image{
//Atributos
private:
	string tipo;
	string comment;
	int largura, altura;
	int color_max;

//Métodos Construtor e Destrutor
public:
	Image(): Image("",0,0,0){};
	Image(string tipo, int largura, int altura, int color_max);
	~Image();

//Métodos Acessores
	void setTipo(string tipo);
	string getTipo();
	void setLargura(int largura);
	int getLargura();
	void setAltura(int altura);
	int getAltura();
	void setColor_max(int color_max);
	int getColor_max();
	void setComment(string comment);
	string getComment();

//Outros
	void readHeader(string filename);
	void readComment(string filename);
	virtual void readData(string filename)=0;
	virtual void imprimeDados()=0;
	virtual void writeImage()=0;
	virtual void decodeComment()=0;
	virtual void decodeImage()=0;
};
#endif
