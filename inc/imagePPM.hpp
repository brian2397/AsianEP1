#ifndef IMAGE_PPM_HPP
#define IMAGE_PPM_HPP
#include <string>
#include <vector>
#include "image.hpp"

using namespace std;

struct Pixel{
  int r, g, b;
};

struct commentKey{
  int begin, messageSize;
  string keyword;
};

class imagePPM: public Image{
private:
  commentKey chave;
  vector<Pixel> pixel_vector;
  vector<int> position_message;
  vector<char> vectorData;
  vector<char> new_alphabet;
  vector<char> message;
public:
  void imprimeDados();
  void readData(string filename);
  void readData2(string filename);
  void writeImage();
  void decodeComment();
  void decodeImage();
};
#endif
