### Descriptografia de Imagens PPM e PGM


1. Coloque a imagem PPM ou PGM na pasta AsianEP1

2. No terminal digite os seguintes comandos:

  ```cd AsianEP1```

  ```  make clean ```

  ``` make  ```

  ``` make run ```

 2.1. Aparecerá na tela:
          Nome do arquivo:

 2.2. Como entrada digite:
        nomedoarquivo.pgm ou nomedoarquivo.ppm

 2.3. Obs: "nomedoarquivo" é o nome da imagem e depois do "." coloque a extensão do arquivo que é pgm ou ppm

3. No terminal irá aparecer:

**Para imagens PPM :**

    * ------DESCRIPTOGRAFIA DA IMAGEM PPM------
    * Tipo:
    * Comentário:  
    * Largura :   Altura:
    * Color_max:
    * MENSAGEM DESCRIPTOGRAFADA:

**Para imagens PGM :**

    * ------DESCRIPTOGRAFIA DA IMAGEM PGM------
    * Tipo:
    * Comentário:  
    * Largura :   Altura:
    * Tons de cinza:
    * MENSAGEM DESCRIPTOGRAFADA:
