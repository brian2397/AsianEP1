#include <iostream>
#include <fstream>
#include "image.hpp"

Image::Image(string tipo, int largura, int altura, int color_max){
	this->tipo=tipo;
	this->largura=largura;
	this->altura=altura;
	this->color_max=color_max;
}

Image::~Image(){
	
}

void Image::setTipo(string tipo){
	this->tipo=tipo;
}

string Image::getTipo(){
	return tipo;
}

void Image::setLargura(int largura){
	this->largura=largura;
}

int Image::getLargura(){
	return largura;
}

void Image::setAltura(int altura){
	this->altura=altura;
}

int Image::getAltura(){
	return altura;
}

void Image::setColor_max(int color_max){
	this->color_max=color_max;
}

int Image::getColor_max(){
	return color_max;
}

void Image::setComment(string comment){
	this->comment=comment;
}

string Image::getComment(){
	return comment;
}

void Image::readHeader(string filename){
	ifstream file;
	file.open(filename, ios::in|ios::binary);

	if(!file.is_open()){
		cout<<"--FILE DONT OPEN--"<<endl;
		exit(1);
	}
//Pega o tipo, largura, altura e color_max/tons de cinza
	else{
		readComment(filename); // Já chama a função para ler comentário
		int bufferSize=1023, count=0;
		char buffer[bufferSize];
		while(true){
			file.getline(buffer, bufferSize);
			if(buffer[0]=='#'){
				file.getline(buffer,bufferSize);
				continue;
			}
			count++;
			switch(count){
				case 1:
					this->tipo=buffer;
					while(true){
						file.getline(buffer,bufferSize,' ');
						if(buffer[0]=='#'){
							file.getline(buffer,bufferSize);
							continue;
						}
						count++;
						switch(count){
							case 2:
								this->largura=atoi(buffer);
								file.getline(buffer,bufferSize);
								this->altura=atoi(buffer);
								break;
							}
						break;
					}
					break;
				case 3:
					this->color_max=atoi(buffer);
					break;
				case 4:
					break;
			}
		if(count==4) break;
		}
	}
	file.close();
}
//Pega o comentário;
void Image::readComment(string filename){
	ifstream file;
	file.open(filename);

	if(!file.is_open()){
		cout<<"--FILE DONT OPEN--"<<endl;
		exit(1);
	}

	else{
		int bufferSize=1023, count=0;
		char buffer[bufferSize];
		while(true){
			file.getline(buffer, bufferSize);
			if(buffer[0]=='#'){
				this->comment=buffer;
				break;
			}
			count++;
			if(count==4){
				cout << "NÃO HÁ COMENTÁRIO" << endl;
				this->comment="";
				break;
			}
		}
	}
	file.close();
}
