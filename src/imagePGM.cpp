#include <iostream>
#include <bits/stdc++.h>
#include <fstream>
#include <string>
#include "imagePGM.hpp"

void imagePGM::imprimeDados(){
    cout<<"Tipo: "<<getTipo()<<endl;
    cout<<"Comentário: "<<getComment()<<endl;
    cout<<"Largura: "<<getLargura()<<" ";
    cout<<"Altura: "<<getLargura()<<endl;
    cout<<"Tons de cinza: "<<getColor_max()<<endl;
}

void imagePGM::readData(string filename){
  char caracter;
  ifstream file;
  file.open(filename,ios::in|ios::binary);
  int dataSize=getAltura()*getLargura(), count=0;
  while(true){
    file.get(caracter);
    if(caracter=='\n') count++;
    switch(count){
      case 1:
        break;
      case 2:
        break;
      case 3:
        break;
      case 4:
        for(int i=0; i<dataSize; i++){
          file.get(caracter);
          vectorData2.push_back(caracter);
        }
        break;
    }
    if(count==4) break;
  }
  file.close();
}

//Verifica se está pegando os dados certos;
void imagePGM::writeImage(){
  ofstream file;
  file.open("newImage.pgm", std::ofstream::out);
  file << getTipo() <<"\n";
  file << getLargura() << " " << getAltura() << "\n";
  file << getColor_max() << endl;
  for(int i=0; i< getAltura()*getLargura(); i++){
    file << vectorData2[i];
  }
  file.close();
}

void imagePGM::decodeComment(){
  if(getComment()==""){
    cout << "NÃO EXISTE MENSAGEM ESCONDIDA" << endl;
  }

  else{
//Pega o valor de onde começa a mensagem;
    int i=0;
    char token[100];
    while(true){
      if(getComment()[i]==' '){
        for(int j=0; j<i; j++){
          token[j]=getComment()[j+1];
        }
        break;
      }
      i++;
    }
    chave2.begin2=atoi(token);
//Pega o valor do tamanho da mensagem;
    int k=i+1, m=0;
    char token2[100];
    while(true){
      if(getComment()[k]==' '){
        for(int l=i+1; l<k; l++){
          token2[m]=getComment()[l];
          m++;
        }
        break;
      }
      k++;
    }
    chave2.messageSize2=atoi(token2);
//Pega o valor dos N deslocamentos da Cifra de César
    int second_space=k+1, n=0, cSize=getComment().size();
    char token3[100];
    for(int o=second_space; o<cSize; o++){
      token3[n]=getComment()[o];
      n++;
    }
    chave2.N=atoi(token3);
  }
}

void imagePGM::decodeImage(){
//position_message2 pega as posições dos caracteres da mensagem;
  int inicio=chave2.begin2;
  int fim=chave2.begin2+chave2.messageSize2;
  for(int i=inicio; i<fim; i++){
    position_message2.push_back(vectorData2[i]);
  }
//Pega os caracteres da mensagem de acordo com a sua posição;
  int pmSize = position_message2.size();
  for(int k=0; k<pmSize; k++){
     char message_caracter = position_message2[k];
     char temp = position_message2[k];
  //Verifica se é letra maiúscula;
     if(message_caracter>64 && message_caracter<91){
       message_caracter = position_message2[k] - chave2.N;
       if(temp>64 && message_caracter<64) message_caracter+=26;
     }
  //Verifica se é letra minúscula;
     if(message_caracter>96 && message_caracter<123){
       message_caracter = position_message2[k] - chave2.N;
       if(temp>96 && message_caracter<96) message_caracter+=26;
     }
  //char a = position_message2[k] - chave2.N;
    message2.push_back(message_caracter);
  }
  //Mostra a mensagem na tela;
  int mSize=message2.size();
  for(int l=0; l<mSize; l++){
   cout<<message2[l];
  }
  cout<<endl;
}
