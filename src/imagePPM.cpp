#include <iostream>
#include <bits/stdc++.h>
#include <fstream>
#include <string>
#include "imagePPM.hpp"

void imagePPM::imprimeDados(){
  cout<<"Tipo: "<<getTipo()<<endl;
  cout<<"Comentário: "<<getComment()<<endl;
  cout<<"Largura: "<<getLargura()<<" ";
  cout<<"Altura: "<<getAltura()<<endl;
  cout<<"Color max: "<<getColor_max()<<endl;
}

//Pega os dados em forma de char para ver está pegando certo;
void imagePPM::readData(string filename){
  char caracter;
  ifstream file;
  file.open(filename,ios::in|ios::binary);
  int size=getAltura()*getLargura()*3, count=0;
  while(true){
    file.get(caracter);
    if(caracter=='\n') count++;
    switch(count){
      case 1:
        break;
      case 2:
        break;
      case 3:
        break;
      case 4:
        for(int i=0; i<size; i++){
          file.get(caracter);
          vectorData.push_back(caracter);
        }
        break;
    }
    if(count==4) break;
  }
  file.close();
}
//Pega os dados em forma de UNSIGNED char para pegar os RGB;
void imagePPM::readData2(string filename){
  ifstream file;
  file.open(filename.c_str(), ios::in|ios::binary);
  int size = getAltura()*getLargura();
  unsigned char buffer[3];
  int count=0;
  char caracter;
  while(true){
    file.get(caracter);
    if(caracter=='\n') count++;
    switch(count){
      case 1:
         break;
       case 2:
         break;
       case 3:
         break;
       case 4:
         for(int i=0; i<size; i++){
           file.read(reinterpret_cast<char *>(buffer), 3);
           Pixel temp;
           temp.r = buffer[0];
           temp.g = buffer[1];
           temp.b = buffer[2];
           pixel_vector.push_back(temp);
         }
         break;
     }
     if(count==4) break;
   }
   file.close();
}

//Verifica se está pegando os dados certos;
void imagePPM::writeImage(){
  ofstream file;
  file.open("newImage.ppm", std::ofstream::out);
  file << getTipo() <<"\n";
  file << getLargura() << " " << getAltura() << "\n";
  file << getColor_max() << endl;

  for(int i=0; i< getAltura()*getLargura()*3; i++){
    file << vectorData[i];
  }

  file.close();
}

void imagePPM::decodeComment(){
  if(getComment()==""){
    cout << "NÃO EXISTE MENSAGEM ESCONDIDA" << endl;
  }
//Pega o valor de onde começa a mensagem;
  else{
    int i=0;
    char token[100];
    while(true){
      if(getComment()[i]==' '){
        for(int j=0; j<i; j++){
          token[j]=getComment()[j+1];
        }
        break;
      }
      i++;
    }
    chave.begin=atoi(token);
//Pega o valor do tamanho da mensagem;
    int k=i+1, m=0;
    char token2[100];
    while(true){
      if(getComment()[k]==' '){
        for(int l=i+1; l<k; l++){
          token2[m]=getComment()[l];
          m++;
        }
        break;
      }
      k++;
    }
    chave.messageSize=atoi(token2);
//Pega a palavra chave da Keyword Cipher;
    int second_space=k+1, n=0, cSize=getComment().size();
    char token3[200];
    for(int o=second_space; o<cSize; o++){
      token3[n]=getComment()[o];
      n++;
    }
    chave.keyword=token3;
  }
}

void imagePPM::decodeImage(){
//Pega a posição dos caracteres da mensagem, somando o último algarismo do R, G, e B de cada pixel
  int inicio=chave.begin;
  int fim=chave.begin+chave.messageSize;
  for(int i=inicio; i<fim; i++){
    position_message.push_back(pixel_vector[i].r%10 + pixel_vector[i].g%10 + pixel_vector[i].b%10);
  }
//Adiciona a palavra chave no novo alfabeto
  int kSize = chave.keyword.size();
  for(int j=0; j< kSize; j++){
    new_alphabet.push_back(chave.keyword[j]);
  }
  char cAlphabet[] = {'a','b','c','d','e','f',
		'g','h','i','j','k','l','m','n','o','p',
		'q','r','s','t','u','v','w','x','y','z'};
//Adiciona o resto do alfabeto depois da palavra chave
  for(int k=0; k<26; k++){
    int naSize=new_alphabet.size(), x=0;
    for(int f=0; f<naSize; f++){
      if(new_alphabet[f]==cAlphabet[k]) x++;
    }
    if(x==0)  new_alphabet.push_back(cAlphabet[k]);
  }
//Com as posições, adiciona os caracteres na mensagem
  int pmSize = position_message.size();
  for(int l=0; l<pmSize; l++){
    int pos=position_message[l];
    if(pos==0) message.push_back(' ');
    else if(pos>=26) {
      pos-=26;
      message.push_back(new_alphabet[pos-1]);
    }
    else message.push_back(new_alphabet[pos-1]);
  }
//Imprime a mensagem na tela;
  int mSize=message.size();
  for(int q=0; q<mSize; q++){
    cout << message[q];
  }
  cout << endl;
}
