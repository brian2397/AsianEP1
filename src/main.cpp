#include <iostream>
#include <fstream>
#include "image.hpp"
#include "imagePPM.hpp"
#include "imagePGM.hpp"

using namespace std;

int main(int argc, char ** argv){
	string filename;
	cout<<"Nome do arquivo: ";
	cin>>filename;
	string tipe;
	ifstream file;
	file.open(filename);
	int bufferSize=1023,count=0;
	char buffer[bufferSize];
	while(true){
		file.getline(buffer, bufferSize);
		if(buffer[0]=='#'){
			continue;
		}
		count++;
		switch(count){
			case 1:
				tipe=buffer;
				break;
			case 2:
				break;
		}
		if(count==2) break;
	}
	file.close();

	if(tipe=="P6"){
		cout<<"------DESCRIPTOGRAFIA DA IMAGEM PPM------"<<endl;
		imagePPM imagem_ppm;
		imagem_ppm.readHeader(filename);
		imagem_ppm.readData(filename);
		imagem_ppm.readData2(filename);
		imagem_ppm.imprimeDados();
		imagem_ppm.decodeComment();
		cout<<"MENSAGEM DESCRIPTOGRAFADA: ";
		imagem_ppm.decodeImage();
		cout<<endl;
		imagem_ppm.writeImage();
	}
	else if(tipe=="P5"){
		cout<<"------DESCRIPTOGRAFIA DA IMAGEM PGM------"<<endl;
		imagePGM imagem_pgm;
		imagem_pgm.readHeader(filename);
		imagem_pgm.readData(filename);
		imagem_pgm.imprimeDados();
		imagem_pgm.decodeComment();
		cout<<"MENSAGEM DESCRIPTOGRAFADA: ";
		imagem_pgm.decodeImage();
		cout<<endl;
		imagem_pgm.writeImage();
	}
	return 0;
}
